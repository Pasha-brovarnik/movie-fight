const autoCompleteConfig = {
	renderOption({ Poster, Title }) {
		const imgSrc = Poster === 'N/A' ? '' : Poster;
		return `
            <img src="${imgSrc}" />
            ${Title}
        `;
	},
	inputValue(movie) {
		return movie.Title;
	},
	async fetchData(searchTerm) {
		const res = await axios.get('https://www.omdbapi.com/', {
			params: {
				apikey: '4a682a75',
				s: searchTerm,
			},
		});
		if (res.data.Error) return [];
		return res.data.Search;
	},
};

createAutoComplete({
	...autoCompleteConfig,
	root: document.querySelector('#left-autocomplete'),
	onOptionSelect(movie) {
		document.querySelector('.tutorial').classList.add('is-hidden');
		onMovieSelect(movie, document.querySelector('#left-summary'), 'left');
	},
});

createAutoComplete({
	...autoCompleteConfig,
	root: document.querySelector('#right-autocomplete'),
	onOptionSelect(movie) {
		document.querySelector('.tutorial').classList.add('is-hidden');
		onMovieSelect(movie, document.querySelector('#right-summary'), 'right');
	},
});
let leftMovie;
let rightMovie;
const onMovieSelect = async ({ imdbID }, summaryElement, side) => {
	const res = await axios.get('https://www.omdbapi.com/', {
		params: {
			apikey: '4a682a75',
			i: imdbID,
		},
	});
	summaryElement.innerHTML = movieTemplate(res.data);

	if (side === 'left') {
		leftMovie = res.data;
	} else {
		rightMovie = res.data;
	}

	if (leftMovie && rightMovie) runComparison();
};

const runComparison = () => {
	const leftSideStats = document.querySelectorAll('#left-summary .notification');
	const rightSideStats = document.querySelectorAll(
		'#right-summary .notification'
	);

	leftSideStats.forEach((leftStat, i) => {
		const rightStat = rightSideStats[i];
		const leftSideVal = parseInt(leftStat.dataset.value);
		const rightSideVal = parseInt(rightStat.dataset.value);

		if (rightSideVal > leftSideVal) {
			leftStat.classList.remove('is-primary');
			leftStat.classList.add('is-warning');
		} else {
			rightStat.classList.remove('is-primary');
			rightStat.classList.add('is-warning');
		}
	});
};

const movieTemplate = (movieDetails) => {
	const metaScore = parseInt(movieDetails.Metascore);
	const imdbRating = parseFloat(movieDetails.imdbRating);
	const imdbVotes = parseInt(movieDetails.imdbVotes.replace(/,/g, ''));
	const awards = movieDetails.Awards.split(' ').reduce((prev, word) => {
		const value = parseInt(word);
		if (isNaN(value)) {
			return prev;
		} else {
			return prev + value;
		}
	}, 0);

	return `
        <article class="media">
            <figure class="media-left">
                <p class="image">
                    <img src="${movieDetails.Poster}" />
                </p>
            </figure>
            <div class="media-content"
                <div class="content"
                    <h1>${movieDetails.Title}</h1>
                    <h4>${movieDetails.Genre}</h4>
                    <p>${movieDetails.Plot}</p>
                </div>
            </div>
        </article>
        <article data-value=${awards} class="notification is-primary">
            <p class="title">${movieDetails.Awards}</p>
            <p class="subtitle">Awards</p>
        </article>
        <article data-value=${metaScore} class="notification is-primary">
            <p class="title">${movieDetails.Metascore}</p>
            <p class="subtitle">Metascore</p>
        </article>
        <article data-value=${imdbRating} class="notification is-primary">
            <p class="title">${movieDetails.imdbRating}</p>
            <p class="subtitle">IMDB Rating</p>
        </article>
        <article data-value=${imdbVotes} class="notification is-primary">
            <p class="title">${movieDetails.imdbVotes}</p>
            <p class="subtitle">IMDB Votes</p>
        </article>
    `;
};
