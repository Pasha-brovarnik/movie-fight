const createAutoComplete = ({
	root,
	renderOption,
	onOptionSelect,
	inputValue,
	fetchData,
}) => {
	root.innerHTML = `
        <input class="input" placeholder="Search" />
        <div class="dropdown">
            <div class="dropdown-menu">
					 <div class="dropdown-content results"></div>
            </div>
        </div>
    `;

	const input = root.querySelector('input');
	const dropdown = root.querySelector('.dropdown');
	const resultsWrapper = root.querySelector('.results');

	const onInput = async ({ target }) => {
		const items = await fetchData(target.value);

		if (!target.value.length) return dropdown.classList.remove('is-active');

		if (!items.length) {
			resultsWrapper.innerHTML = '';
			dropdown.classList.add('is-active');
			const option = document.createElement('a');
			option.classList.add('dropdown-item', 'no-results');
			option.innerHTML = 'Try a different search';
			resultsWrapper.appendChild(option);
			return;
		}

		resultsWrapper.innerHTML = '';
		dropdown.classList.add('is-active');
		for (const item of items) {
			const option = document.createElement('a');

			option.classList.add('dropdown-item');
			option.innerHTML = renderOption(item);
			option.addEventListener('click', () => {
				dropdown.classList.remove('is-active');
				input.value = inputValue(item);
				onOptionSelect(item);
			});

			resultsWrapper.appendChild(option);
		}
	};

	input.addEventListener('input', debounce(onInput));

	document.addEventListener('click', (event) => {
		if (!root.contains(event.target)) dropdown.classList.remove('is-active');
	});
};
